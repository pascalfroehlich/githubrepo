package at.spengergasse.githubrepo;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class GithubrepoApplication {

    private static HttpClient client = HttpClient.newHttpClient();

    private static HttpRequest searchForRepositories(String searchTerm) {
        return HttpRequest.newBuilder()
                .uri(URI.create(String.format("https://api.github.com/search/repositories?q=%s", searchTerm)))
                .build();
    }

    public static void searchForRepositorysByNameSequential(List<String> repositorySearchnames) {
        System.out.println("  _____                             _   _       _ \n" +
                " / ____|                           | | (_)     | |\n" +
                "| (___   ___  __ _ _   _  ___ _ __ | |_ _  __ _| |\n" +
                " \\___ \\ / _ \\/ _` | | | |/ _ \\ '_ \\| __| |/ _` | |\n" +
                " ____) |  __/ (_| | |_| |  __/ | | | |_| | (_| | |\n" +
                "|_____/ \\___|\\__, |\\__,_|\\___|_| |_|\\__|_|\\__,_|_|\n" +
                "                | |                               \n" +
                "                |_|                               \n");
        Long timeBefore = System.currentTimeMillis();
        repositorySearchnames.stream()
                .sequential()
                .forEach(searchname -> client.sendAsync(searchForRepositories(searchname), HttpResponse.BodyHandlers.ofString())
                        .thenApply(HttpResponse::body)
                        .thenAccept(System.out::println)
                        .join());

        System.out.println("Sequential braucht: " + String.valueOf(System.currentTimeMillis() - timeBefore) + " ms\n");
    }

    public static void searchForRepositorysByNameParallel(List<String> repositorySearchnames) {
        System.out.println("\n" +
                " _____                _ _      _ \n" +
                "|  __ \\              | | |    | |\n" +
                "| |__) |_ _ _ __ __ _| | | ___| |\n" +
                "|  ___/ _` | '__/ _` | | |/ _ \\ |\n" +
                "| |  | (_| | | | (_| | | |  __/ |\n" +
                "|_|   \\__,_|_|  \\__,_|_|_|\\___|_|\n" +
                "                                 \n");
        Long timeBefore = System.currentTimeMillis();
        repositorySearchnames.stream()
                .parallel()
                .forEach(searchname -> client.sendAsync(searchForRepositories(searchname), HttpResponse.BodyHandlers.ofString())
                        .thenApply(HttpResponse::body)
                        .thenAccept(System.out::println)
                        .join());

        System.out.println("Parallel braucht: " + String.valueOf(System.currentTimeMillis() - timeBefore) + " ms");
    }


    public static void main(String[] args) {
        ArrayList<String> repositorySearchterms = new ArrayList<>();
        repositorySearchterms.add("SpringBoot");
        repositorySearchterms.add("spring");
        repositorySearchterms.add("Hibernate");
        repositorySearchterms.add("MongoDB");
        repositorySearchterms.add("MySQL");
        searchForRepositorysByNameSequential(repositorySearchterms);
        searchForRepositorysByNameParallel(repositorySearchterms);

    }

}
